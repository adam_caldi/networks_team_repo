# UDPClientPEX1.py
#
# Author: Adam Caldi, Celeste Trevino August 2016

"""
Documentation: We looked up several python syntax questions on stackexchange.com and used the answers from the questions
from previous years to write the code. We also looked up information pertaining to git and bit bucket on bitbucket
because we had problems with our repository deleting code or not working all together.
"""


import socket
import sys
import time
import wave


my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_name = '127.0.0.1' #'127.0.0.1'   # '127.0.0.1'  # 'dfcs-raptor'    # '54.214.120.204'
server_port = 4240
command = "null"
songToStream = "null"
# message = "null"

# songList = []                        # future development

my_socket.settimeout(5.0)              # set socket timeout


def interactionlist():
    """
        Contacts the server; if successful responds with server response and address.
         If it is unable to connect to the server it returns one of three responses.
        If the connection times out then it returns that the server has not responded in a timely manner.
        If the socket's connection was reset it returns that it was reset.  All other errors are
        relayed as "unexpected error".
        :param: none
        :return: a string with connection status
        """

    my_socket.sendto(command.encode('UTF-8'), (server_name, server_port))

    buffer_size = 4096
    # (response, server_address) = my_socket.recvfrom(buffer_size)
    # print(response)

    try:
        (response, server_address) = my_socket.recvfrom(buffer_size)
        print("Server response: ", response.decode('UTF-8'))
        print("Server address:  ", server_address)
    except socket.timeout:
        print('The server has not responded in a timely manner. The server may be down.')
    except ConnectionResetError:
        print("The socket's connection was reset.")
    except:
        # All other errors are caught here
        print("Unexpected error:", sys.exc_info()[0])

    print("\n")


def interactionstream(command, songToStream):
    """
        This function takes the user input and sends it to the server
        :param: the command and the song to stream given by the user
        :return: packets to the song or an error if the connection fails
        """
    my_socket.sendto(command.encode('UTF-8'), (server_name, server_port))
    packetnum = 0
    buffer_size = 10000
    temp = bytearray()

    try:
        with open(songToStream + '.mp3', 'bw+') as f:
            while my_socket.recvfrom(buffer_size):
                (response, server_address) = my_socket.recvfrom(buffer_size)

                # cuts off new line and ascii header bits from stream and concatenates arrays to temp
                temp = b"".join([temp, response[12:]])

                # temp = b"".join([temp, my_socket.recvfrom(buffer_size)[0][12:]])

                print(response)
                packetnum += 1
                if response == b'STREAM_DONE':
                    f.write(temp)
                    print("--------Stream Complete--------")
                    print("Number of packets processed: " + str(packetnum))
                    break

        print()
        # print("Server address:  ", server_address)

    except socket.timeout:
        print('The server has not responded in a timely manner. The server may be down.')
    except ConnectionResetError:
        print("The socket's connection was reset.")
    except:
        # All other errors are caught here
        print("Unexpected error:", sys.exc_info()[0])

    print("\n")


print("Please enter a command.")
message = input("Commands are as follows: \n 1: list \n 2: stream \n 3: quit \n >>>> ")


while command == "null":

    # exits if user wants to quit
    if message == "quit":
        sys.exit("Exiting")

    # sends server a request to list the songs
    elif message == "list":
        command = "LIST_REQUEST"
        interactionlist()

    # sends the server a request to stream and allows user input of which song
    elif message == "stream":
        songToStream = input("Enter the name of the song you want to stream: ")
        command = "START_STREAM\n" + songToStream
        print("-------Attempting Stream-------")
        # print(command)

        # calls function to send start stream command & song to stream
        interactionstream(command, songToStream)


    else:
        # catches all other errors for invalid commands
        print("Invalid command")

    # init()

    # resets the command to null to allow the user to enter in a new command
    command = "null"
    print("Please enter a command.")
    message = input("Commands are as follows: \n 1: list \n 2: stream \n 3: quit \n >>> ")


my_socket.close()

# Delete the socket from memory to again reclaim memory resources.
del my_socket