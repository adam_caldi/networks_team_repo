# UDPServer.py
#
# Author: Adam Caldi, Celeste Trevino August 2016

"""
Documentation: We looked up several python syntax questions on stackexchange.com and used the answers from the questions
from previous years to write the code. We also looked up information pertaining to git and bit bucket on bitbucket
because we had problems with our repository deleting code or not working all together.
"""


import socket, sys, time


my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

bitrate_lookup = {0: -1, 1: 32000, 2: 40000, 3: 48000, 4: 56000, 5: 64000, 6: 80000, 7: 96000, 8: 112000, 9: 128000,
                  10: 160000, 11: 192000, 12: 224000, 13: 256000, 14: 320000, 15: -1}

samplerate_lookup = {0: 44100, 1: 48000, 2: 32000, 3: -1}

server_port = 4240
my_socket.bind( ('', server_port) )


buffer_size = 4096

print(sys.argv[0])
requestCounter = 0


def mp3framer(mp3name):
    f = open('Server mp3s/'+mp3name, "rb")
    data = f.read()

    print(data)
    data = bytearray(data)

    looper = frame_pos = data.find(b'\xff\xfb', 0)
    print(looper)

    packet = 0
    counter = 0

    while looper < len(data):
            time.sleep(.01)
            frame_pos = data.find(b'\xff\xfb', looper)
            # print(data[frame_pos:frame_pos+4])
            print(frame_pos)

            samplerate1 = (data[frame_pos+2]) >> 2
            # print(samplerate1)
            samplerate1 = (samplerate1) & 3
            # print(samplerate1)
            samplerate = samplerate_lookup[samplerate1]
            # print(samplerate)

            bitrate1 = (data[frame_pos+2]) >> 4
            # print(bitrate1)
            bitrate1 = bitrate1 & 15
            bitrate = bitrate_lookup[bitrate1]
            # print(bitrate)

            padding1= (data[frame_pos + 2]) >> 1
            padding = padding1 & 1


            # print(padding)


            if (padding == 1):
                frameSize = 144 * bitrate / samplerate + 1
            else:
                frameSize = (144 * bitrate / samplerate)
            # print(frameSize)

            frame_end = int(frame_pos) + int(frameSize)
            packet = data[frame_pos:frame_end]
            packet = b'STREAM_DATA\n'+packet
            looper += int(frameSize)

            my_socket.sendto(packet, (client_address))

            print(packet)
            counter += 1
            print(counter)


while (True):
    print(requestCounter)

    (data, client_address) = my_socket.recvfrom(buffer_size)
    # print("Message: ", data)
    print("Message: ", data.decode('UTF-8'))
    print("Client address: ", client_address)


    if (data == b'LIST_REQUEST'):
        my_socket.sendto(b'\nSuzanne Vega - Toms Diner.mp3\nBilly Joel - We Didn\'t Start the Fire.mp3',(client_address))

    # print(data)
    dataHeader = data[:12]
    # print(dataHeader)
    print(dataHeader.decode('UTF-8'))
    dataData = data[13:]
    # print(dataData)
    if (dataHeader.decode('UTF-8') ==  'START_STREAM'):
        print("Sending")
        mp3framer(dataData)
        my_socket.sendto(b'STREAM_DONE', (client_address))






    requestCounter += 1

# If the above loop was not infinite, the socket's resources should be reclaimed
my_socket.close()
del my_socket